import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FirstViewComponent } from './components/first-view/first-view.component';

import { ReactiveFormsModule } from '@angular/forms';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { SecondViewComponent } from './components/second-view/second-view.component';
import { routing, appRoutingProviders } from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    FirstViewComponent,
    SecondViewComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AngularFontAwesomeModule,
    routing
  ],
  providers: [
    appRoutingProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
