import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';

@Component({
  selector: 'app-first-view',
  templateUrl: './first-view.component.html',
  styleUrls: ['./first-view.component.css']
})
export class FirstViewComponent implements OnInit {

  // data for notes to developer
  notes: Array<string> = [
    'Aparece el tutor de  izquierda a derecha',
    'Aparece el formulario',
    'Se activa el botón "ingresar!" para ir a la siguiente pantalla',
    'En la parte inferior esta la navegación del curso, junto con el seguimiento de pantallas'
  ]

  stateAudio = false;

  constructor() { }

  ngOnInit() {
    localStorage.setItem('viewNumber', '1');
  }



  changeTextAudio(){
    this.stateAudio = !this.stateAudio;
  }

  // methods for login
  onLogin(form?: NgForm){
    console.log(form.value)
  }





  // validators with ngx-forms
  // Validator for login User
  formLogin = new FormGroup({
    nombre: new FormControl('', Validators.required),
    edad: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{1,2}/)]),
    email: new FormControl('', [Validators.required, Validators.pattern(/[^@]+@[^\.]+\..+/)]),
    genero: new FormControl('', Validators.required),
    audio: new FormControl('')
  });

}