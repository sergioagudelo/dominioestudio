import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirstViewComponent } from './components/first-view/first-view.component';
import { SecondViewComponent } from './components/second-view/second-view.component';


const appRoutes: Routes = [
  {path: '', component: FirstViewComponent},
  {path: 'firstView', component: FirstViewComponent},
  {path: 'secondView', component: SecondViewComponent}
];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
